# REAMDE
## How to run a n.eko instance like, super easily and for free for a long time!

Hi! I made a StackScript on Linode to run [n.eko](https://github.com/nurdism/neko) on a uh. Linode.

It's really fuckin' easy. All's you gotta do is: 
First, go to [Linode](https://linode.com/techtangents) and sign up for an account. This link is the affiliate link of a Youtuber I like, lol, but it will get you $20 for free. Also check his [channel](https://www.youtube.com/user/AkBKukU) out, he's the best kinda nerd. ANYWAY. 

Once you've signed up, go to the menu thinger on the side. Look for "StackScripts:"

![StackScriptsMenu](/1.png "StackScripts is located on the sidebar menu. In the image it is second from the bottom, however on the actual menu it is fourth from the bottom. It is the 10th item down.")

Then. go to "Community StackScripts" and search for "lumpenfreude" or "n.eko." IDK how exactly it'll turn up. I'll figure it out after someone tries this guide the first time lol! This is how it looks to me: 

![StackScriptThing](/2.png "This image shows how my StackScript looks to me, it has four columns, 'StackScript,' 'Total Deploys,' 'Last Revision,' and 'Compatible Images.' The entries are, respectively, 'lumpenfreude/n.eko - StackScript to automatically launch a n.eko [groupwatching software] instance on a Linode. Sorta like ...', '7', '2020-05-19', and 'debian10.' There is a 3 dot menu on the right.")

Anyways! Click the 3 dots. Click "Deploy New Linode" You can try to put in a different password/admin password. It probably won't work lol. But it might! Who knows? Pick a browser! This doesn't seem to have much of an impact on performance in my experience but you can experiment. It does vary a bit version to version. 

As far as Linod Plan, the Linode 4GB servers seem to work pretty decently for low bitrate 720p content (~2 mpbs), 2GB is enough for SD content. Scaling upwards is rough... even super high cost servers don't seem to handle 1080p THAT well. Seems to be a limitation of the software. But 720p is more than enough considering the application. IDK. Anyways! Make sure you pick a SERVER LOCATION, set a ROOT PASSWORD (you will almost certainly not need this), and click CREATE! 

Then...

Wait a while. Like. Give it a good couple of minutes. It's chugging away. It's running a shitload of scripts. Thinking. Hard. Once it starts, tho, get yr IP address, like so: 

![IPThing](/3.jpg "This shows the entire Linode dashboard with a large overlay that says IP! FREELY! and a small arrow pointing to the IP address. the IP address starts with the number 69. As such, there is also a sticker of a man with black hair and a silly expression on his face holding up an okay sign with the words NICE INFO! printed below him.")

after a few minutes (idk like 2 or 3) it should be up and running! Give it a try! 
